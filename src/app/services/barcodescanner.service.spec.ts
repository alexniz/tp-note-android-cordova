import { TestBed } from '@angular/core/testing';

import { BarcodescannerService } from './barcodescanner.service';

describe('BarcodescannerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BarcodescannerService = TestBed.get(BarcodescannerService);
    expect(service).toBeTruthy();
  });
});
