import { TestBed } from '@angular/core/testing';
import { OkbarcodescannerserviceService } from './okbarcodescannerservice.service';

describe('OkbarcodescannerserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OkbarcodescannerserviceService = TestBed.get(OkbarcodescannerserviceService);
    expect(service).toBeTruthy();
  });
});
