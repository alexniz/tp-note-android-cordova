import { Injectable } from '@angular/core';
import { BarcodeScannerOptions,BarcodeScanner } from "@ionic-native/barcode-scanner/ngx";

class Qr{
  texte : string;
  format : string;
  date: Date;
}


@Injectable({
  providedIn: 'root'
})
export class OkbarcodescannerserviceService {
  public QrTable: Array<Qr> = [];  

  constructor(
    public barcodeScanner: BarcodeScanner,
    ) { }

    scanCode() {

      this.barcodeScanner.scan().then(barcodeData => {
          this.QrTable.unshift({
            texte: barcodeData.text,
            format: barcodeData.format,
            date: new Date(),
          });
          return;
        })
        .catch(err => {
          console.log("Error", err);
        });
    }
}
