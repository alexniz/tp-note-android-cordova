import { Component } from '@angular/core';
import {
  BarcodeScannerOptions,
  BarcodeScanner
} from "@ionic-native/barcode-scanner/ngx";
import { OkbarcodescannerserviceService } from './../services/barcodescanner-service/okbarcodescannerservice.service';
import { ToastController } from '@ionic/angular';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {


  constructor(public barcodeScannerService: OkbarcodescannerserviceService) {

  }

  scanCode() {
    this.barcodeScannerService.scanCode();
   
  }
 

}
